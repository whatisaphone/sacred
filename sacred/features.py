import cv2
import numpy as np

from sacred.playground import Playground, Trackbar


def find_counter(image: np.mat) -> np.mat:  # pragma: no cover
    return find_counter_surf(image)


def find_counter_hsv(image: np.mat) -> np.mat:  # pragma: no cover
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    @Playground.run
    class MyPlayground(Playground):
        def __init__(self, window_name: str) -> None:
            super().__init__(window_name)
            self.min_h = Trackbar(window_name, 'min_h', 0, 255, 0)
            self.max_h = Trackbar(window_name, 'max_h', 0, 255, 255)
            self.min_s = Trackbar(window_name, 'min_s', 0, 255, 0)
            self.max_s = Trackbar(window_name, 'max_s', 0, 255, 255)
            self.min_v = Trackbar(window_name, 'min_v', 0, 255, 0)
            self.max_v = Trackbar(window_name, 'max_v', 0, 255, 255)
            self.kernel_size = Trackbar(window_name, 'kernel_size', 1, 19)
            self.iterations = Trackbar(window_name, 'iterations', 1, 19)

        def tick(self) -> np.mat:
            low = np.array([self.min_h.value, self.min_s.value, self.min_v.value])
            high = np.array([self.max_h.value, self.max_s.value, self.max_v.value])
            mask = cv2.inRange(hsv, low, high)

            kernel_size = self.kernel_size.value | 1
            kernel = np.ones((kernel_size, kernel_size), np.uint8)
            mask = cv2.erode(mask, kernel, iterations=self.iterations.value)
            mask = cv2.dilate(mask, kernel, iterations=self.iterations.value)
            mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
            return cv2.add(image, mask)

    counter = cv2.inRange(hsv, np.array([0, 72, 64]), np.array([24, 96, 80]))
    kernel = np.ones((3, 3), np.uint8)
    counter = cv2.erode(counter, kernel, iterations=1)
    counter = cv2.dilate(counter, kernel, iterations=1)
    return counter


def find_counter_lines(image: np.mat) -> np.mat:  # pragma: no cover
    @Playground.run
    class MyPlayground(Playground):
        def __init__(self, window_name: str) -> None:
            super().__init__(window_name)
            self.canny_t1 = Trackbar(window_name, 'canny_t1', 0, 255, 50)
            self.canny_t2 = Trackbar(window_name, 'canny_t2', 0, 255, 150)
            self.hough_rho = Trackbar(window_name, 'hough_rho', 1, 100)
            self.hough_theta = Trackbar(window_name, 'hough_theta', 0.1, 10, 1, fractional=10)
            self.hough_thresh = Trackbar(window_name, 'hough_thresh', 0, 200, 100)
            self.hough_min_len = Trackbar(window_name, 'hough_min_len', 0, 200, 10)
            self.hough_max_gap = Trackbar(window_name, 'hough_max_gap', 0, 20, 5)

        def tick(self) -> np.mat:
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            edges = cv2.Canny(gray, self.canny_t1.value, self.canny_t2.value)
            print(self.hough_theta.value)  # noqa: T003
            lines = cv2.HoughLinesP(edges, self.hough_rho.value,
                                    np.pi / 180 * self.hough_theta.value,
                                    self.hough_thresh.value,
                                    minLineLength=self.hough_min_len.value,
                                    maxLineGap=self.hough_max_gap.value)

            edges = cv2.cvtColor(edges, cv2.COLOR_GRAY2BGR)
            if lines is not None:
                for (x1, y1, x2, y2), in lines:
                    cv2.line(edges, (x1, y1), (x2, y2), (255, 0, 255), 2)
            return cv2.addWeighted(image, 0.5, edges, 1, 0)


def find_counter_gftt(image: np.mat) -> np.mat:  # pragma: no cover
    @Playground.run
    class MyPlayground(Playground):
        def __init__(self, window_name: str) -> None:
            super().__init__(window_name)
            self.max_corners = Trackbar(window_name, 'maxCorners', 0, 100)
            self.quality_level = Trackbar(window_name, 'qualityLevel', 0.01, 1, fractional=100)
            self.min_distance = Trackbar(window_name, 'minDistance', 0, 50, 10)

        def tick(self) -> np.mat:
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            corners = cv2.goodFeaturesToTrack(
                gray, self.max_corners.value, self.quality_level.value, self.min_distance.value)

            output = image.copy()

            for (x, y), in corners:
                cv2.circle(output, (x, y), 9, (0, 0, 255))

            return output


def find_counter_surf(image: np.mat) -> np.mat:  # pragma: no cover
    @Playground.run
    class MyPlayground(Playground):
        def __init__(self, window_name: str) -> None:
            super().__init__(window_name)
            self.threshold = Trackbar(window_name, 'threshold', 0, 50000, 4000)

        def tick(self) -> np.mat:
            gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

            surf = cv2.xfeatures2d.SURF_create(self.threshold.value)
            kp, ds = surf.detectAndCompute(gray, None)
            print(len(kp))  # noqa: T003

            output = cv2.drawKeypoints(gray, kp, None, (0, 0, 255),
                                       cv2.DrawMatchesFlags_DRAW_RICH_KEYPOINTS)

            return output
