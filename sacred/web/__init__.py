from enum import Enum
from queue import Queue
from threading import Thread
from wsgiref.simple_server import WSGIServer, make_server  # noqa: F401

from pyramid.config import Configurator
from pyramid.request import Request
from pyramid.response import Response


class Webapp:
    def __init__(self, port: int) -> None:
        self.port = port
        self.server = None  # type: WSGIServer
        self.events = Queue()  # type: Queue[WebappEvent]

    def start(self) -> None:
        Thread(target=self._thread_entry).start()

    def destroy(self) -> None:
        self.server.shutdown()

    def _thread_entry(self) -> None:
        with Configurator() as config:
            config.add_route('root', '/')
            config.add_view(self._root, route_name='root')

            config.add_route('force-incident', '/force-incident', request_method='POST')
            config.add_view(self._force_incident, route_name='force-incident')

            app = config.make_wsgi_app()

        self.server = make_server('0.0.0.0', self.port, app)
        self.server.serve_forever()

    def _root(self, _request: Request) -> Response:  # pragma: no cover
        return Response('<form method=post action=force-incident><button>Force incident')

    def _force_incident(self, _request: Request) -> Response:  # pragma: no cover
        self.events.put(WebappEvent.FORCE_INCIDENT)
        return Response('{"status":"ok"}', content_type='application/json')


class WebappEvent(Enum):
    FORCE_INCIDENT = 'force-incident'
