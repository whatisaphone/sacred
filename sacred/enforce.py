from sacred.config import Config


def run_enforce(config: Config) -> None:
    for enforcer in config.enforcers:
        enforcer.handle_incident()
