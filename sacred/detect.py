from pathlib import Path
from typing import Iterable, Tuple

import cv2
import numpy as np

from sacred.opencv import outline
from sacred.utils import RollingMedian
from sacred.video import Frame


class CatDetector:
    def __init__(self) -> None:
        mask_filename = Path(__file__).parent.parent / 'assets' / 'counter-mask.png'
        self.mask = cv2.imread(str(mask_filename), cv2.IMREAD_GRAYSCALE)
        assert self.mask is not None

        self.motion_detector = MotionDetector(32)
        self.masked_threshold = 0.0005
        self.full_threshold = 0.025
        self.full_height_threshold = 0.5

        self.masked_trigger_wait = 0.1
        self.full_trigger_wait = 30

        self.mask_outline = outline(self.mask, (191, 0, 0))
        self.not_mask = cv2.bitwise_not(self.mask)

        self.last_masked_trigger_value = False
        self.last_masked_trigger_changed = 0.0
        self.last_full_trigger_time = 0.0

    def feed(self, frame: Frame) -> Tuple[bool, np.mat]:
        image = cv2.cvtColor(frame.image, cv2.COLOR_BGR2GRAY)
        image = cv2.GaussianBlur(image, (5, 5), 0)
        self.motion_detector.feed(frame.moment, image)

        masked = cv2.bitwise_and(image, self.mask)
        masked_motion = self.motion_detector.detect_motion(image, self.mask, masked)
        full_motion = self.motion_detector.detect_motion(image, None, None)

        masked_trigger = masked_motion.motion >= self.masked_threshold
        if masked_trigger != self.last_masked_trigger_value:
            self.last_masked_trigger_value = masked_trigger
            self.last_masked_trigger_changed = frame.moment

        full_trigger = full_motion.motion >= self.full_threshold \
            or full_motion.bounds.h / frame.image.shape[0] > self.full_height_threshold
        if full_trigger:
            self.last_full_trigger_time = frame.moment

        result = masked_trigger and not full_trigger \
            and self.last_masked_trigger_changed <= frame.moment - self.masked_trigger_wait \
            and not self.pause_time_remaining(frame.moment)

        diag = self.diag(frame.moment, image, masked, masked_motion, full_motion)

        return result, diag

    def diag(self, moment: float, image: np.mat, masked: np.mat,
             masked_motion: 'MotionData', full_motion: 'MotionData') -> np.mat:
        inv_masked = cv2.bitwise_and(image, self.not_mask)
        faded = cv2.addWeighted(masked, 1, inv_masked, 0.5, 0)
        diag = cv2.cvtColor(faded, cv2.COLOR_GRAY2BGR) + self.mask_outline

        cv2.putText(diag, 'Full motion:', (15, 25), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))
        self.motion_detector.draw_diag(diag, masked_motion, (0, 255, 0), (160, 25))

        cv2.putText(diag, 'Masked motion:', (15, 40), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 255))
        self.motion_detector.draw_diag(diag, full_motion, (0, 0, 255), (160, 40))

        cv2.putText(diag, 'Paused:', (15, 55), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255))
        pause_time = self.pause_time_remaining(moment)
        cv2.putText(diag, 'for {:.1f} s'.format(pause_time) if pause_time > 0 else 'no',
                    (160, 55), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255))

        return diag

    def pause_time_remaining(self, moment: float) -> float:
        time = self.full_trigger_wait - (moment - self.last_full_trigger_time)
        return time if time > 0 else 0


class Rect:
    def __init__(self, x: float, y: float, w: float, h: float) -> None:
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    @classmethod
    def from_cv2(cls, xywh: Tuple[float, float, float, float]) -> 'Rect':
        return Rect(*xywh)


class MotionData:
    def __init__(self, motion: float, threshold: np.mat, bounds: Rect) -> None:
        self.motion = motion
        self.threshold = threshold
        self.bounds = bounds


class MotionDetector:
    def __init__(self, threshold: int) -> None:
        self.threshold = threshold
        self.rolling_median = RollingMedian()

    def feed(self, moment: float, image: np.mat) -> None:
        self.rolling_median.feed(moment, image)

    def detect_motion(self, image: np.mat, mask: np.mat = None,
                      masked_image: np.mat = None) -> MotionData:
        median = self.rolling_median.median()

        if mask is not None:
            median = cv2.bitwise_and(median, mask)
            image = masked_image

        delta = cv2.absdiff(median, image)
        _, threshold = cv2.threshold(delta, self.threshold, 255, cv2.THRESH_BINARY)

        delta_count = cv2.countNonZero(threshold)
        pixel_count = threshold.shape[0] * threshold.shape[1]
        motion = delta_count / pixel_count
        bounds = cv2.boundingRect(cv2.findNonZero(threshold))

        return MotionData(motion, threshold, Rect.from_cv2(bounds))

    def draw_diag(self, diag: np.mat, data: MotionData,
                  color: Iterable[int], origin: Tuple[int, int]) -> None:
        r = data.bounds
        cv2.rectangle(diag, (r.x, r.y), (r.x + r.w, r.y + r.h), color, 1)

        # Slow but useful for debugging:
        # thresh = colorize(cv2.cvtColor(data.threshold, cv2.COLOR_GRAY2BGR), color)
        # cv2.addWeighted(diag, 1, thresh, 0.25, 0, diag)
        # cv2.add(diag, outline(data.threshold, color), diag)

        cv2.putText(diag, '{:.2%}'.format(data.motion), origin, cv2.FONT_HERSHEY_PLAIN, 1, color)
