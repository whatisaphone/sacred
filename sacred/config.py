from pathlib import Path
from typing import Any, Dict, List

import yaml

from sacred.enforcer import (
    AudioEnforcer, Enforcer, ExecEnforcer, KankunEnforcer, SloppyEnforcer, SquirtEnforcer,
)
from sacred.video import OpenCVVideoSource, ThreadedVideoSource, VideoSource


class Config:
    def __init__(self, *, sentry_dsn: str, video_url: str, snapshots_path: Path,
                 snapshots_preroll: int, snapshots_postroll: int,
                 show_window: bool, enforcers: List[Enforcer]) -> None:
        self.sentry_dsn = sentry_dsn
        self.video_url = video_url
        self.snapshots_path = snapshots_path
        self.snapshots_preroll = snapshots_preroll
        self.snapshots_postroll = snapshots_postroll
        self.show_window = show_window
        self.enforcers = enforcers
        self.opencv_preview_window_name = 'sacred'

    @classmethod
    def load(cls) -> 'Config':
        path = (Path(__file__).parent.parent / 'config.yaml')
        with path.open(encoding='utf-8') as f:
            data = yaml.safe_load(f)

        return cls(
            sentry_dsn=data['sentry']['dsn'],
            video_url=data['video']['url'],
            snapshots_path=Path(data['snapshots']['path']),
            snapshots_preroll=int(data['snapshots']['preroll']),
            snapshots_postroll=int(data['snapshots']['postroll']),
            show_window=data['window']['show'],
            enforcers=list(map(cls._load_enforcer, data['enforcers'])),
        )

    @classmethod
    def _load_enforcer(cls, data: Dict[str, Any]) -> Enforcer:
        return SloppyEnforcer(cls._load_enforcer_unwrapped(data))

    @classmethod
    def _load_enforcer_unwrapped(cls, data: Dict[str, Any]) -> Enforcer:
        if data['type'] == 'kankun':
            return KankunEnforcer(data['url'])
        elif data['type'] == 'squirt':
            return SquirtEnforcer(data['url'], data['from'], data['to'])
        elif data['type'] == 'audio':
            return AudioEnforcer(Path(data['file']))
        elif data['type'] == 'exec':
            return ExecEnforcer(data['command'])
        else:  # pragma: no cover
            raise KeyError

    def destroy(self) -> None:
        for enforcer in self.enforcers:
            enforcer.destroy()

    def build_video_source(self) -> VideoSource:  # pragma: no cover
        return ThreadedVideoSource(OpenCVVideoSource(self.video_url))
