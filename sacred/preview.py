from sacred.config import Config
from sacred.opencv import ImageWindow


def run_preview(config: Config) -> None:  # pragma: no cover
    with \
            config.build_video_source() as source, \
            ImageWindow(config.opencv_preview_window_name) as window:
        while window.is_visible():
            frame = source.fetch_frame()
            window.show(frame.image)
