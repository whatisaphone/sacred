from datetime import datetime
from unittest import TestCase

from sacred.tests.fixtures import EndOfFixture
from sacred.tests.mocks import FakeVideoSource


class FakeVideoSourceTests(TestCase):
    def test_after_len_elapsed_should_raise(self) -> None:
        expected = 2 * 5

        count = 0
        with FakeVideoSource(fps=2, length=5) as source:
            while True:
                try:
                    source.fetch_frame()
                    count += 1
                except EndOfFixture:
                    break

        self.assertEqual(count, expected)

    def test_time(self) -> None:
        with FakeVideoSource(fps=2, length=2) as source:
            source.fetch_frame()

            self.assertEqual(datetime.now(), datetime(2000, 1, 1, 0, 0, 0))

            source.fetch_frame()

            self.assertEqual(datetime.now(), datetime(2000, 1, 1, 0, 0, 0, 500_000))

            source.fetch_frame()

            self.assertEqual(datetime.now(), datetime(2000, 1, 1, 0, 0, 1))

            source.fetch_frame()

            self.assertEqual(datetime.now(), datetime(2000, 1, 1, 0, 0, 1, 500_000))

            self.assertRaises(EndOfFixture, source.fetch_frame)

            self.assertTrue(datetime.now().year > 2000)
