from unittest import TestCase
from unittest.mock import patch

from sacred.utils import FPSCounter, TimeLimitedBuffer


class TimeLimitedBufferTests(TestCase):
    def test_should_respect_max_length(self) -> None:
        buffer = TimeLimitedBuffer[int](3)
        for i in range(10):
            buffer.push(i, i)

        self.assertSequenceEqual(buffer.contents, [(6, 6), (7, 7), (8, 8), (9, 9)])


class FPSCounterTests(TestCase):
    def test_simple(self) -> None:
        cases = [
            ([0.0, 2.0, 4.0], 0.5),
            ([0.0, 1.0, 2.0, 3.0], 1),
            ([0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0], 2),
        ]
        for ticks, expected in cases:
            with self.subTest(ticks):
                counter = FPSCounter()

                for tick in ticks:
                    with patch('sacred.utils.monotonic', lambda: tick):
                        counter.tick()

                self.assertEqual(counter.fps(), expected)
