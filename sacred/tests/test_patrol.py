from itertools import chain
from unittest import TestCase
from unittest.mock import Mock

from sacred.detect import CatDetector
from sacred.opencv import NullWindow
from sacred.patrol import Enforcer, Patrol
from sacred.snapshots import Snapshotter
from sacred.tests.fixtures import EndOfFixture, create_test_image
from sacred.tests.mocks import FakeVideoSource


class PatrolTests(TestCase):
    def setUp(self) -> None:
        self.source = FakeVideoSource(fps=1, length=100)
        self.detector = Mock(spec_set=CatDetector)
        self.snapshotter = Mock(spec_set=Snapshotter)
        self.enforcer = Mock(spec_set=Enforcer)
        self.patrol = Patrol(self.source, self.detector, self.snapshotter,
                             NullWindow(), [self.enforcer])
        self.image = create_test_image()

    def tearDown(self) -> None:
        self.patrol.destroy()
        self.snapshotter.destroy()
        self.source.destroy()

    def test_when_cat_not_detected_should_not_trigger_offense(self) -> None:
        self.detector.feed.side_effect = [(False, self.image)] * 100

        self.assertRaises(EndOfFixture, self.patrol.run)

        self.assertEqual(self.enforcer.handle_incident.call_count, 0)

    def test_when_cat_detected_should_trigger_offense(self) -> None:
        self.detector.feed.side_effect = [(False, self.image)] * 99 + [(True, self.image)] * 1

        self.assertRaises(EndOfFixture, self.patrol.run)

        self.assertEqual(self.enforcer.handle_incident.call_count, 1)

    def test_should_only_trigger_once_every_so_often(self) -> None:
        self.detector.feed.side_effect = chain(
            [(True, self.image)] * 4,  # Incident 1
            [(False, self.image)] * 2,
            [(True, self.image)] * 4,  # Still incident 1
            [(False, self.image)] * 40,
            [(True, self.image)] * 1,  # Incident 2
            [(False, self.image)] * 49,
        )

        self.assertRaises(EndOfFixture, self.patrol.run)

        self.assertEqual(self.enforcer.handle_incident.call_count, 2)
