from io import StringIO
from pathlib import Path
from textwrap import dedent
from unittest import TestCase
from unittest.mock import patch

from sacred.config import Config
from sacred.enforcer import (
    AudioEnforcer, ExecEnforcer, KankunEnforcer, SloppyEnforcer, SquirtEnforcer,
)


class ConfigTests(TestCase):
    def test_load(self) -> None:
        file = StringIO(dedent("""\
        sentry:
          dsn: test://sentry/1234
        video:
          url: https://example.com/video
        snapshots:
          path: /tmp/snapshots
          preroll: 29556
          postroll: 53617
        window:
          show: true
        enforcers:
        - type: kankun
          url: https://kankun.test
        - type: squirt
          url: https://squirt.test
          from: 22
          to: 33
        - type: audio
          file: /tmp/sound.wav
        - type: exec
          command: /bin/true
        """))

        with patch('io.open', return_value=file):
            config = Config.load()

        try:
            self.assertEqual(config.sentry_dsn, 'test://sentry/1234')

            self.assertEqual(config.video_url, 'https://example.com/video')

            self.assertEqual(config.snapshots_path, Path('/tmp/snapshots'))
            self.assertEqual(config.snapshots_preroll, 29556)
            self.assertEqual(config.snapshots_postroll, 53617)

            self.assertEqual(config.show_window, True)

            assert isinstance(config.enforcers[0], SloppyEnforcer)
            assert isinstance(config.enforcers[0].inner, KankunEnforcer)
            self.assertEqual(config.enforcers[0].inner.url, 'https://kankun.test')

            assert isinstance(config.enforcers[1], SloppyEnforcer)
            assert isinstance(config.enforcers[1].inner, SquirtEnforcer)
            self.assertEqual(config.enforcers[1].inner.url, 'https://squirt.test')
            self.assertEqual(config.enforcers[1].inner.from_angle, 22)
            self.assertEqual(config.enforcers[1].inner.to_angle, 33)

            assert isinstance(config.enforcers[2], SloppyEnforcer)
            assert isinstance(config.enforcers[2].inner, AudioEnforcer)
            self.assertEqual(config.enforcers[2].inner.path, Path('/tmp/sound.wav'))

            assert isinstance(config.enforcers[3], SloppyEnforcer)
            assert isinstance(config.enforcers[3].inner, ExecEnforcer)
            self.assertEqual(config.enforcers[3].inner.command, '/bin/true')
        finally:
            config.destroy()
