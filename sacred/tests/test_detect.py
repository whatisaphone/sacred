from typing import List, Optional
from unittest import TestCase

import cv2

from sacred.detect import CatDetector
from sacred.tests.fixtures import EndOfFixture, FixtureVideoSource, FrameFilenameInfo

SHOW_VIDEO = True
WINDOW_NAME = 'sacred_test_window'


class CatDetectorTests(TestCase):
    def test_video(self) -> None:
        cases = [
            ('20170826-jump-by-dishwasher-3', ['175614', '175615', '175616']),
            ('20170827-jump-by-fridge-2', ['213629', '213630']),
            ('20170828-climb-by-sink', ['051435', '051436']),
            ('20170829-walking-on-floor', None),
            ('20170831-making-a-sandwich', None),
            ('20171228-walking-past-counter', None),
            ('20171231-adjusting-camera', None),
            ('20171231-laptop-on-counter', None),
        ]

        if SHOW_VIDEO:  # pragma: no cover
            cv2.namedWindow(WINDOW_NAME)

        for name, result in cases:
            with self.subTest(name):
                self._test_video(name, result)

        if SHOW_VIDEO:  # pragma: no cover
            cv2.destroyWindow(WINDOW_NAME)

    def _test_video(self, name: str, expected: Optional[List[str]]) -> None:
        """
        Run a single video test.

        If `expected` is None, no trigger should occur. If `expected` is given,
        the trigger should occur in one of the passed HHMMSS.
        """
        detector = CatDetector()
        with FixtureVideoSource(name) as source:
            trigger = _find_trigger_frame(source, detector)
            if expected is None:
                self.assertIsNone(trigger)
            else:
                assert trigger is not None
                self.assertIn(trigger.hhmmss, expected)


def _find_trigger_frame(source: FixtureVideoSource,
                        detector: CatDetector) -> Optional[FrameFilenameInfo]:
    while True:
        try:
            info, frame = source.fetch_annotated_frame()
        except EndOfFixture:
            break

        result, diag = detector.feed(frame)

        if SHOW_VIDEO:  # pragma: no cover
            cv2.imshow(WINDOW_NAME, diag)
            cv2.waitKey(1)

        if result:
            return info

    return None
