from unittest import TestCase
from unittest.mock import Mock

from sacred.enforce import run_enforce
from sacred.enforcer import Enforcer
from sacred.tests.fixtures import create_test_config


class RunEnforceTests(TestCase):
    def test_should_enforce(self) -> None:
        enforcer = Mock(spec_set=Enforcer)
        config = create_test_config(enforcers=[enforcer])
        run_enforce(config)
        self.assertEqual(enforcer.handle_incident.call_count, 1)
