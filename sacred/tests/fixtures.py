import re
from pathlib import Path
from typing import List, Tuple

import cv2
import numpy as np

from sacred.config import Config
from sacred.enforcer import Enforcer
from sacred.video import Frame, VideoSource


class FrameFilenameInfo:
    def __init__(self, hhmmss: str, offset: int, frame: int) -> None:
        self.hhmmss = hhmmss
        self.offset = offset
        """The number of seconds from midnight."""
        self.frame = frame
        """The 0-indexed frame number within the current second."""

    def __repr__(self) -> str:  # pragma: no cover
        return '{}({!r}, {!r}, {!r})'.format(
            self.__class__.__name__, self.hhmmss, self.frame, self.offset)

    @classmethod
    def parse(cls, filename: str) -> 'FrameFilenameInfo':
        h, m, s, f = re.match(r'\A(\d{2})-?(\d{2})-?(\d{2})-(\d{1,2})[.]jpg\Z', filename).groups()
        offset = (int(h) * 60 + int(m)) * 60 + int(s)
        return cls(h + m + s, offset, int(f))


class FixtureVideoSource(VideoSource):
    def __init__(self, name: str) -> None:
        super().__init__()
        self.path = Path(__file__).parent / 'fixtures' / 'detector' / name
        self.filenames = sorted(self.path.iterdir())
        self.infos = [FrameFilenameInfo.parse(f.name) for f in self.filenames]
        self.files = zip(self.filenames, self.infos)

    def fetch_frame(self) -> Frame:
        raise NotImplementedError

    def fetch_annotated_frame(self) -> Tuple[FrameFilenameInfo, Frame]:
        try:
            filename, info = next(self.files)
        except StopIteration:
            raise EndOfFixture
        fps = _detect_fps_for_hhmmss(self.infos, info.hhmmss)
        image = cv2.imread(str(filename))
        assert image is not None
        frame = Frame(info.offset + info.frame / fps, image)
        return info, frame


class EndOfFixture(Exception):
    pass


def _detect_fps_for_hhmmss(infos: List[FrameFilenameInfo], hhmmss: str) -> int:
    return max(i.frame for i in infos if i.hhmmss == hhmmss) + 1


def create_test_config(enforcers: List[Enforcer]) -> Config:
    return Config(
        sentry_dsn='test://sentry/1234',
        video_url='http://video.test',
        snapshots_path=Path('/tmp/snapshots'),
        snapshots_preroll=5,
        snapshots_postroll=5,
        show_window=False,
        enforcers=enforcers,
    )


def create_test_frame(moment: float) -> Frame:
    return Frame(moment, create_test_image())


def create_test_image() -> np.mat:
    return np.array([[0]])
