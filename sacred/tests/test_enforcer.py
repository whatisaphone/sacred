from unittest import TestCase
from unittest.mock import patch

import requests_mock

from sacred.enforcer import KankunEnforcer, SquirtEnforcer
from sacred.tests.mocks import mock_timer_start


class KankunEnforcerTests(TestCase):
    def setUp(self) -> None:
        self.mock_requests = requests_mock.Mocker()
        self.mock_requests.start()

    def tearDown(self) -> None:
        self.mock_requests.stop()

    def test_handle_incident(self) -> None:
        enforcer = KankunEnforcer('http://example.com/kankun')
        self.mock_requests.get('http://example.com/kankun/cgi-bin/json.cgi')

        with patch('threading.Timer.start', mock_timer_start):
            enforcer.handle_incident()

        self.assertEqual(self.mock_requests.call_count, 2)
        self.assertEqual(self.mock_requests.request_history[0].url,
                         'http://example.com/kankun/cgi-bin/json.cgi?set=on')
        self.assertEqual(self.mock_requests.request_history[1].url,
                         'http://example.com/kankun/cgi-bin/json.cgi?set=off')


class SquirtEnforcerTests(TestCase):
    def setUp(self) -> None:
        self.mock_requests = requests_mock.Mocker()
        self.mock_requests.start()

    def tearDown(self) -> None:
        self.mock_requests.stop()

    def test_handle_incident(self) -> None:
        enforcer = SquirtEnforcer('http://squirt.test', 33, 99)
        self.mock_requests.post('http://squirt.test/sweep')

        enforcer.handle_incident()

        self.assertEqual(self.mock_requests.call_count, 2)

        self.assertEqual(self.mock_requests.request_history[0].url,
                         'http://squirt.test/sweep')
        self.assertEqual(self.mock_requests.request_history[0].json(), {
            'angleFrom': 33,
            'angleTo': 99,
            'time': 500,
            'granularity': 10,
        })

        self.assertEqual(self.mock_requests.request_history[1].url,
                         'http://squirt.test/sweep')
        self.assertEqual(self.mock_requests.request_history[1].json(), {
            'angleFrom': 99,
            'angleTo': 33,
            'time': 500,
            'granularity': 10,
        })
