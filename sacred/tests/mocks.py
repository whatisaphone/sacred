from datetime import datetime, timedelta
from threading import Timer

from freezegun import freeze_time

from sacred.tests.fixtures import EndOfFixture, create_test_frame
from sacred.video import Frame, VideoSource


class FakeVideoSource(VideoSource):
    def __init__(self, *, fps: int, length: int) -> None:
        super().__init__()
        self.start = 0
        self.stop = (self.start + length) * fps - 1
        self.fps = fps
        self.length = length
        self.cur_frame = self.start * fps - 1
        self.freezer = freeze_time(_datetime_for_moment(self.start))
        self.frozen = self.freezer.start()

    def destroy(self) -> None:
        assert datetime.now().year > 2000

    def fetch_frame(self) -> Frame:
        if self.cur_frame >= self.stop:
            self.done()

        self.cur_frame += 1
        moment = self.cur_frame / self.fps
        self.frozen.move_to(_datetime_for_moment(moment))
        frame = create_test_frame(moment)
        return frame

    def done(self) -> None:
        self.freezer.stop()
        raise EndOfFixture


def _datetime_for_moment(moment: float) -> datetime:
    return datetime(2000, 1, 1) + timedelta(seconds=moment)


def mock_timer_start(self: Timer) -> None:
    self.function(*self.args, **self.kwargs)  # type: ignore
