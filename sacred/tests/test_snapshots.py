from datetime import datetime, timedelta
from pathlib import Path
from tempfile import TemporaryDirectory
from unittest import TestCase

import numpy as np
from freezegun import freeze_time

from sacred.snapshots import Snapshotter
from sacred.tests.fixtures import create_test_frame


class SnapshotterTests(TestCase):
    def setUp(self) -> None:
        self.directory = TemporaryDirectory()
        self.path = Path(self.directory.name) / 'snaps'
        self.snapshotter = Snapshotter(self.path, 0.5, 0.5)

    def tearDown(self) -> None:
        self.snapshotter.destroy()
        self.directory.cleanup()

    def test_when_nothing_detected_should_not_save_images(self) -> None:
        self.snapshotter.feed(create_test_frame(0), False)
        self.assertEqual(list(self.path.iterdir()), [])

    def test_when_cat_detected_should_save_images(self) -> None:
        with freeze_time('2000-01-01') as freezer:
            for moment in np.arange(0, 10, 0.25):
                freezer.move_to(datetime(2000, 1, 1) + timedelta(seconds=moment))
                result = 3 <= moment < 4
                self.snapshotter.feed(create_test_frame(moment), result)
                self.snapshotter.flush()

        self.assertEqual([p.name for p in self.path.iterdir()], ['2000-01-01 00-00-03'])

        self.assertEqual([p.name for p in (self.path / '2000-01-01 00-00-03').iterdir()], [
            '00-00-02-2.jpg',
            '00-00-02-3.jpg',
            '00-00-03-0.jpg',
            '00-00-03-1.jpg',
            '00-00-03-2.jpg',
            '00-00-03-3.jpg',
            '00-00-04-0.jpg',
            '00-00-04-1.jpg',
        ])

    def test_with_statement(self) -> None:
        with Snapshotter(self.path, 5, 5):
            pass
