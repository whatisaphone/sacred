from subprocess import run
from unittest import TestCase


class LintTests(TestCase):
    def test_flake8(self) -> None:
        run(['flake8', 'sacred']).check_returncode()

    def test_pylint(self) -> None:
        run(['pylint', 'sacred']).check_returncode()

    def test_mypy(self) -> None:
        run(['mypy', 'sacred']).check_returncode()

    def test_can_import_untested_modules(self) -> None:
        from sacred import debug, features, main, playground, preview
        self.assertIsNotNone(debug)
        self.assertIsNotNone(features)
        self.assertIsNotNone(main)
        self.assertIsNotNone(playground)
        self.assertIsNotNone(preview)
