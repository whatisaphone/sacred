from unittest import TestCase

import cv2

from sacred.opencv import ImageWindow, is_window_visible
from sacred.tests.fixtures import create_test_image


class IsWindowVisibleTests(TestCase):
    def test_true(self) -> None:
        cv2.namedWindow('test')
        self.assertEqual(is_window_visible('test'), True)
        cv2.destroyWindow('test')

    def test_false(self) -> None:
        self.assertEqual(is_window_visible('test'), False)

    def test_show_hide(self) -> None:
        cv2.namedWindow('test')
        cv2.destroyWindow('test')
        self.assertEqual(is_window_visible('test'), False)


class ImageWindowTests(TestCase):
    def test(self) -> None:
        with ImageWindow('ImageWindowTests') as window:
            self.assertEqual(window.is_visible(), True)
            window.show(create_test_image())
