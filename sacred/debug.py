from os import close
from subprocess import run
from tempfile import mkstemp

import cv2
import numpy as np

from sacred.opencv import KEY_ESC, is_window_visible

WINDOW_NAME = 'debug'


def view(image: np.mat) -> None:  # pragma: no cover
    cv2.imshow(WINDOW_NAME, image)
    while is_window_visible(WINDOW_NAME):
        key = cv2.waitKey(1)
        if key == KEY_ESC:
            break


def inspect(image: np.mat) -> None:  # pragma: no cover
    fd, filename = mkstemp('.png')
    close(fd)
    cv2.imwrite(filename, image)

    run([r'C:\Program Files\paint.net\PaintDotNet.exe', filename])
