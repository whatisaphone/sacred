from abc import ABCMeta, abstractmethod
from calendar import timegm
from collections import deque
from datetime import datetime
from time import monotonic
from typing import Any, Generic, List, Optional, Tuple, TypeVar  # noqa: F401

import numpy as np

from sacred.opencv import median

try:  # Waiting for Python 3.6 upgrade
    from typing import Deque  # noqa: F401
except ImportError:  # pragma: no cover
    pass

T = TypeVar('T')
A = TypeVar('A')


class TimeLimitedBuffer(Generic[T]):
    """A buffer that stores data representing a given length of time."""

    def __init__(self, span: float) -> None:
        self.contents = deque()  # type: Deque[Tuple[float, T]]
        self.span = span

    def push(self, position: float, value: T) -> None:
        while self.contents:
            pos, _ = self.contents[0]
            if pos < position - self.span:
                self.contents.popleft()
            else:
                break
        self.contents.append((position, value))


class FPSCounter:
    def __init__(self) -> None:
        self.window = 2
        self.frames = TimeLimitedBuffer[None](self.window)

    def tick(self) -> None:
        self.frames.push(monotonic(), None)

    def fps(self) -> float:
        return (len(self.frames.contents) - 1) / self.window


class Feedable(Generic[T], metaclass=ABCMeta):
    @abstractmethod
    def feed(self, position: float, value: T) -> None:
        pass


class Windower(Generic[T], Feedable[T], metaclass=ABCMeta):
    """A buffer that groups data into windows of a given size."""

    def __init__(self, window_size: float) -> None:
        self.window_size = window_size
        self._active_window_start = 0.0
        self._active_window_stop = 0.0

    def feed(self, position: float, value: T) -> None:
        cur_window_start = position - position % self.window_size
        if cur_window_start != self._active_window_start:
            self._on_advance_window(self._active_window_start, cur_window_start)
            self._active_window_start = cur_window_start
            self._active_window_stop = cur_window_start + self.window_size

        self._on_value(value)

    @abstractmethod
    def _on_advance_window(self, prev_window_start: float, next_window_start: float) -> None:
        pass

    @abstractmethod
    def _on_value(self, value: T) -> None:
        pass


def datetime_to_unix(dt: datetime) -> float:
    return timegm(dt.timetuple())


class RollingMedian(Feedable[np.mat]):
    def __init__(self) -> None:
        self.sink = RollingMedianSink()
        self.layer1 = RollingMedianLayer(30, self.sink)
        self.layer2 = RollingMedianLayer(6, self.layer1)
        self.layer3 = RollingMedianLayer(1, self.layer2)
        self.layers = [self.sink, self.layer1, self.layer2, self.layer3]

    def feed(self, position: float, image: np.mat) -> None:
        self.layers[-1].feed(position, image)

    def median(self) -> np.mat:
        medians = (l.median() for l in self.layers)  # pragma: no branch
        return next(m for m in medians if m is not None)  # pragma: no branch


class Medianator(Feedable[np.mat], metaclass=ABCMeta):
    @abstractmethod
    def median(self) -> Optional[np.mat]:
        pass


class RollingMedianLayer(Medianator, Windower[np.mat]):
    def __init__(self, window_size: float, sink: Feedable[np.mat]) -> None:
        super().__init__(window_size)
        self.sink = sink
        self.images = []  # type: List[np.mat]
        self.cached_median = None  # type: Optional[np.mat]

    def _on_advance_window(self, prev_window_start: float, _next_window_start: float) -> None:
        if self.images:
            self.sink.feed(prev_window_start, median(self.images))
            del self.images[:]
            self.cached_median = None

    def _on_value(self, value: T) -> None:
        self.images.append(value)
        self.cached_median = None

    def median(self) -> Optional[np.mat]:
        if self.cached_median is None:
            if self.images:
                self.cached_median = median(self.images)
        return self.cached_median


class RollingMedianSink(Medianator):
    def __init__(self) -> None:
        super().__init__()
        self.cached_median = None  # type: Optional[np.mat]

    def feed(self, _position: float, value: np.mat) -> None:
        self.cached_median = value

    def median(self) -> Optional[np.mat]:
        return self.cached_median
