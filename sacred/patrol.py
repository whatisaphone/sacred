from queue import Empty
from typing import Any, List, Optional  # noqa: F401

import cv2
import numpy as np

from sacred.config import Config
from sacred.detect import CatDetector
from sacred.enforcer import Enforcer
from sacred.opencv import ImageWindow, NullWindow, Window
from sacred.snapshots import Snapshotter
from sacred.utils import FPSCounter
from sacred.video import VideoSource
from sacred.web import Webapp, WebappEvent


def run_patrol(config: Config) -> None:  # pragma: no cover
    detector = CatDetector()
    with \
            config.build_video_source() as source, \
            Snapshotter(config.snapshots_path, config.snapshots_preroll,
                        config.snapshots_postroll) as snapshotter, \
            create_window(config) as window, \
            Patrol(source, detector, snapshotter, window, config.enforcers) as patrol:
        patrol.run()


def create_window(config: Config) -> Window:
    if not config.show_window:
        return NullWindow()
    return ImageWindow(config.opencv_preview_window_name)


class Patrol:  # Python 3.6: ContextManager['Patrol']
    def __init__(self, source: VideoSource, detector: CatDetector,
                 snapshotter: Snapshotter, window: Window, enforcers: List[Enforcer]) -> None:
        self.source = source
        self.detector = detector
        self.snapshotter = snapshotter
        self.window = window
        self.enforcers = enforcers
        self.fps_counter = FPSCounter()
        self.last_incident = None  # type: Optional[float]

        self.webapp = Webapp(6543)

    def __enter__(self) -> 'Patrol':  # pragma: no cover
        return self

    def __exit__(self, exc_type: Any, exc_value: Any, traceback: Any) -> None:  # pragma: no cover
        self.destroy()

    def destroy(self) -> None:
        self.window.destroy()
        self.webapp.destroy()

    def run(self) -> None:
        self.webapp.start()

        while True:
            self._flush_webapp_events()

            diag = self._process_frame()

            if self.window:  # pragma: no branch
                # Do this before displaying to screen, because `is_visible` is
                # a blocking call and it will waste a lot of time if it's
                # waiting for an image to display before returning.
                if not self.window.is_visible():  # pragma: no cover
                    break

                self._overlay_stats(diag)
                self.window.show(diag)

    def _flush_webapp_events(self) -> None:  # pragma: no cover
        while True:
            try:
                event = self.webapp.events.get(block=False)
            except Empty:
                break

            if event == WebappEvent.FORCE_INCIDENT:
                self._handle_incident()
            else:
                raise AssertionError

    def _process_frame(self) -> np.mat:
        frame = self.source.fetch_frame()
        result, diag = self.detector.feed(frame)
        self.fps_counter.tick()

        self.snapshotter.feed(frame, result)
        if result:
            self._handle_motion(frame.moment)

        return diag

    def _overlay_stats(self, diag: np.mat) -> None:
        color = 0, 255, 0
        cv2.putText(diag, '{:.1f} fps'.format(self.fps_counter.fps()),
                    (diag.shape[1] - 100, 25), cv2.FONT_HERSHEY_PLAIN, 1, color)

    def _handle_motion(self, moment: float) -> None:
        if self.last_incident is None:
            is_incident = True
        else:
            is_incident = moment - self.last_incident > 10

        self.last_incident = moment

        if is_incident:
            self._handle_incident()

    def _handle_incident(self) -> None:
        for enforcer in self.enforcers:
            enforcer.handle_incident()
