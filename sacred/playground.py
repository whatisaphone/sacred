from abc import ABCMeta, abstractmethod
from typing import Any, Callable

import cv2
import numpy as np

from sacred.opencv import KEY_ESC, is_window_visible

WINDOW_NAME_CONTROLS = 'playground controls'
WINDOW_NAME_IMAGE = 'playground image'


IntOrFloat = Any


class Trackbar:  # pragma: no cover
    def __init__(self, window_name: str, name: str, min_value: IntOrFloat, max_value: IntOrFloat,
                 value: IntOrFloat = None, *, fractional: IntOrFloat = None) -> None:
        self.window_name = window_name
        self.name = name
        self.min = min_value
        self.fractional = fractional

        cv2_value = int((value or min_value) * (fractional or 1))
        cv2_count = int((max_value - min_value) * (fractional or 1))
        cv2.createTrackbar(name, window_name, cv2_value, cv2_count, lambda _: None)

    @property
    def value(self) -> IntOrFloat:
        raw = cv2.getTrackbarPos(self.name, self.window_name)
        if self.fractional is not None:
            return self.min + raw / self.fractional
        return self.min + raw

    @value.setter
    def value(self, value: IntOrFloat) -> None:
        cv2.setTrackbarPos(self.name, self.window_name, value)


class Playground(metaclass=ABCMeta):  # pragma: no cover
    def __init__(self, window_name: str) -> None:
        pass

    @abstractmethod
    def tick(self) -> np.mat:
        pass

    @staticmethod
    def run(playground_cls: Callable[[str], 'Playground']) -> None:
        cv2.namedWindow(WINDOW_NAME_CONTROLS, cv2.WINDOW_NORMAL)
        cv2.namedWindow(WINDOW_NAME_IMAGE, cv2.WINDOW_AUTOSIZE)

        playground = playground_cls(WINDOW_NAME_CONTROLS)

        while is_window_visible(WINDOW_NAME_CONTROLS) and is_window_visible(WINDOW_NAME_IMAGE):
            image = playground.tick()
            cv2.imshow(WINDOW_NAME_IMAGE, image)

            key = cv2.waitKey(1)
            if key == KEY_ESC:
                break

        cv2.destroyWindow(WINDOW_NAME_CONTROLS)
        cv2.destroyWindow(WINDOW_NAME_IMAGE)
