import json
from abc import ABCMeta, abstractmethod
from enum import Enum
from pathlib import Path
from queue import Queue
from subprocess import run
from threading import Thread, Timer

import requests


class Enforcer(metaclass=ABCMeta):
    @abstractmethod
    def handle_incident(self) -> None:
        pass

    def destroy(self) -> None:
        pass


class SloppyEnforcer(Enforcer):  # pragma: no cover
    """Run an enforcer in a background thread and suppress all errors."""

    class Message(Enum):
        ENFORCE = 'enforce'
        QUIT = 'quit'

    def __init__(self, inner: Enforcer) -> None:
        self.inner = inner
        self.queue = Queue()  # type: Queue[SloppyEnforcer.Message]
        self.thread = Thread(target=self.target)
        self.thread.start()

    def destroy(self) -> None:
        self.queue.put(SloppyEnforcer.Message.QUIT)
        self.thread.join()

    def handle_incident(self) -> None:
        self.queue.put(SloppyEnforcer.Message.ENFORCE)

    def target(self) -> None:
        while True:
            message = self.queue.get()
            if message == SloppyEnforcer.Message.QUIT:
                break
            elif message == SloppyEnforcer.Message.ENFORCE:
                try:
                    self.inner.handle_incident()
                except Exception as e:  # pylint: disable=broad-except
                    print('{} during enforcement'.format(type(e)))  # noqa: T003
            else:
                raise KeyError


class KankunEnforcer(Enforcer):
    """Turn on a Kankun wifi smart plug which has been loaded with the json cgi script."""

    def __init__(self, url: str) -> None:
        self.url = url

    def handle_incident(self) -> None:
        requests.get(self.url + '/cgi-bin/json.cgi?set=on')
        Timer(2, self.off).start()

    def off(self) -> None:
        requests.get(self.url + '/cgi-bin/json.cgi?set=off')


class SquirtEnforcer(Enforcer):
    """Douse kitty with water."""

    def __init__(self, url: str, from_angle: int, to_angle: int) -> None:
        self.url = url
        self.from_angle = from_angle
        self.to_angle = to_angle

    def handle_incident(self) -> None:
        self.rotate(self.from_angle, self.to_angle)
        self.rotate(self.to_angle, self.from_angle)

    def rotate(self, from_angle: int, to_angle: int) -> None:
        requests.post(self.url + '/sweep', json={
            'angleFrom': from_angle,
            'angleTo': to_angle,
            'time': 500,
            'granularity': 10,
        })


class AudioEnforcer(Enforcer):
    """Play a sound."""

    def __init__(self, path: Path) -> None:
        self.path = path

    def handle_incident(self) -> None:  # pragma: no cover
        psh = '(New-Object Media.SoundPlayer {}).PlaySync();'.format(json.dumps(str(self.path)))
        run(['powershell', '-c', psh]).check_returncode()


class ExecEnforcer(Enforcer):
    """Run a command."""

    def __init__(self, command: str) -> None:
        self.command = command

    def handle_incident(self) -> None:  # pragma: no cover
        run(self.command).check_returncode()
