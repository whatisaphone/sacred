import sys
from typing import List

from sacred.config import Config
from sacred.enforce import run_enforce
from sacred.patrol import run_patrol
from sacred.preview import run_preview
from sacred.telemetry import init_logging, init_sentry


def main() -> None:  # pragma: no cover
    init_logging()

    config = Config.load()

    init_sentry(config.sentry_dsn)

    try:
        run_main(config, sys.argv[1:])
    except KeyboardInterrupt:
        to_stderr('Quitting.')
    finally:
        config.destroy()


def run_main(config: Config, args: List[str]) -> None:  # pragma: no cover
    command = args[0] if args else None
    if command == 'patrol':
        run_patrol(config)
    elif command == 'preview':
        run_preview(config)
    elif command == 'enforce':
        run_enforce(config)
    else:
        to_stderr('Invalid command.')
        sys.exit(1)


def to_stderr(text: str) -> None:  # pragma: no cover
    print(text, file=sys.stderr)  # noqa: T001
