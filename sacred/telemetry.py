import logging
import warnings
from logging import StreamHandler

from raven import Client
from raven.handlers.logging import SentryHandler


def init_logging() -> None:  # pragma: no cover
    warnings.simplefilter('default')

    logger = logging.getLogger()
    logger.addHandler(StreamHandler())


def init_sentry(sentry_dsn: str) -> None:  # pragma: no cover
    logger = logging.getLogger()
    client = Client(sentry_dsn)
    logger.addHandler(SentryHandler(client))
