from abc import ABCMeta, abstractmethod
from enum import Enum
from queue import Queue
from threading import Thread
from typing import Any, Iterable, Tuple, TypeVar  # noqa: F401

import cv2
import numpy as np

KEY_ESC = 27


def is_window_visible(window_name: str) -> bool:
    return cv2.getWindowProperty(window_name, cv2.WND_PROP_AUTOSIZE) != -1


def median(images: Iterable[np.mat]) -> np.mat:
    return np.median(np.dstack(images), axis=2).astype('uint8')


def colorize(image: np.mat, color: Iterable[int]) -> np.mat:
    for ci, ch in enumerate(color):
        image[:, :, ci] = cv2.multiply(image[:, :, ci], ch / 255)
    return image


def outline(image: np.mat, color: Iterable[int]) -> np.mat:
    kernel = np.ones((3, 3), np.uint8)
    result = cv2.morphologyEx(image, cv2.MORPH_GRADIENT, kernel)
    return colorize(cv2.cvtColor(result, cv2.COLOR_GRAY2BGR), color)


Self = TypeVar('Self')


class Window(metaclass=ABCMeta):  # Python 3.6: ContextManager['ImageWindow']
    def __enter__(self: Self) -> Self:
        return self

    def __exit__(self, exc_type: Any, exc_value: Any, traceback: Any) -> None:
        self.destroy()

    def destroy(self) -> None:
        pass

    @abstractmethod
    def show(self, image: np.mat) -> None:
        pass

    def is_visible(self) -> bool:
        return True


class NullWindow(Window):
    def show(self, image: np.mat) -> None:
        pass


class ImageWindow(Window):
    class Message(Enum):
        IMAGE = 'image'
        IS_VISIBLE = 'is_visible'
        QUIT = 'quit'

    def __init__(self, name: str) -> None:
        self.name = name
        self.messages = Queue()  # type: Queue[Tuple[np.mat, Any]]
        self.thread = Thread(target=self.target)
        self.thread.start()

    def destroy(self) -> None:
        self.messages.put((ImageWindow.Message.QUIT, None))
        self.thread.join()

    def show(self, image: np.mat) -> None:
        self.messages.put((ImageWindow.Message.IMAGE, image))

    def is_visible(self) -> bool:
        queue = Queue()  # type: Queue[bool]
        self.messages.put((ImageWindow.Message.IS_VISIBLE, queue))
        return queue.get()

    def target(self) -> None:
        cv2.namedWindow(self.name)
        while True:
            message, data = self.messages.get()
            if message == ImageWindow.Message.IMAGE:
                image = data  # type: np.mat
                cv2.imshow(self.name, image)
                cv2.waitKey(1)
            elif message == ImageWindow.Message.IS_VISIBLE:
                queue = data  # type: Queue[bool]
                queue.put(is_window_visible(self.name))
            elif message == ImageWindow.Message.QUIT:
                break
            else:  # pragma: no cover
                raise KeyError
        cv2.destroyWindow(self.name)
