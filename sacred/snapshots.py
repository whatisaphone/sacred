from datetime import datetime, timedelta
from enum import Enum
from pathlib import Path
from queue import Queue
from threading import Event, Thread
from typing import Any, Optional, Tuple  # noqa: F401

import cv2
import numpy as np

from sacred.utils import TimeLimitedBuffer, datetime_to_unix
from sacred.video import Frame


class Snapshotter:  # Python 3.6: ContextManager['Snapshotter']
    def __init__(self, path: Path, preroll: float, postroll: float) -> None:
        self.queue = Queue()  # type: Queue[Tuple[SnapshotterInner.Message, Any]]
        inner = SnapshotterInner(self.queue, path, preroll, postroll)
        self.thread = Thread(target=inner.target)
        self.thread.start()

    def __enter__(self) -> 'Snapshotter':
        return self

    def __exit__(self, exc_type: Any, exc_value: Any, traceback: Any) -> None:
        self.destroy()

    def feed(self, frame: Frame, result: bool) -> None:
        self.queue.put((SnapshotterInner.Message.FRAME, (frame, result)))

    def flush(self) -> None:
        event = Event()
        self.queue.put((SnapshotterInner.Message.FLUSH, event))
        event.wait()

    def destroy(self) -> None:
        self.queue.put((SnapshotterInner.Message.QUIT, None))
        self.thread.join()


class SnapshotterInner:
    class Message(Enum):
        FRAME = 'frame'
        FLUSH = 'flush'
        QUIT = 'quit'

    def __init__(self, queue: 'Queue[Tuple[Message, Any]]',
                 path: Path, preroll: float, postroll: float) -> None:
        self.queue = queue
        self.path = path
        self.preroll = preroll
        self.postroll = postroll
        self.buffer = TimeLimitedBuffer[np.mat](preroll + 1)
        self.current_group = None  # type: Optional[Group]

        self.path.mkdir(exist_ok=True)

    def target(self) -> None:
        while True:
            message, data = self.queue.get()
            if message == SnapshotterInner.Message.FRAME:
                self.feed(*data)
            elif message == SnapshotterInner.Message.FLUSH:
                event = data  # type: Event
                event.set()
            elif message == SnapshotterInner.Message.QUIT:
                break
            else:  # pragma: no cover
                raise KeyError

    def feed(self, frame: Frame, result: bool) -> None:
        if result and self.current_group is None:
            self.start_group(frame)
        self.buffer.push(frame.moment, frame.image)
        if self.current_group:
            if result:
                self.current_group.reset_postroll(frame.moment)
            filename = self.current_group.advance_filename(frame.moment)
            self.write_frame(filename, frame.image)
            if self.current_group.last_moment + self.postroll <= frame.moment:
                self.finish_group()

    def start_group(self, frame: Frame) -> None:
        now = datetime.now()
        path = self.path / now.strftime('%Y-%m-%d %H-%M-%S')
        path.mkdir()
        self.current_group = Group(now, frame.moment, path)
        self.write_preroll(frame.moment)

    def finish_group(self) -> None:
        self.current_group = None

    def write_preroll(self, reference_point: float) -> None:
        assert self.current_group is not None
        for moment, image in self.buffer.contents:
            filename = self.current_group.advance_filename(moment)
            if moment >= reference_point - self.preroll:
                self.write_frame(filename, image)

    def write_frame(self, filename: str, image: np.array) -> None:
        assert self.current_group is not None
        cv2.imwrite(str(self.current_group.path / filename), image)


class Group:
    def __init__(self, timestamp: datetime, moment: float, path: Path) -> None:
        self.timestamp = timestamp
        self.moment = moment
        self.last_moment = moment
        self.path = path
        self.frame = 0
        self.last_unix = 0

    def advance_filename(self, moment: float) -> str:
        elapsed = moment - self.moment
        timestamp = self.timestamp + timedelta(seconds=elapsed)
        unix = int(datetime_to_unix(timestamp))
        if unix != self.last_unix:
            self.last_unix = unix
            self.frame = 0
        filename = '{:%H-%M-%S}-{}.jpg'.format(timestamp, self.frame)
        self.frame += 1
        return filename

    def reset_postroll(self, moment: float) -> None:
        self.last_moment = moment
