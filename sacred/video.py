import logging
from abc import ABCMeta, abstractmethod
from queue import Empty, Queue
from threading import Thread
from time import monotonic
from typing import Any, TypeVar

import cv2
import numpy as np


class Frame:
    def __init__(self, moment: float, image: np.mat) -> None:
        self.moment = moment
        self.image = image


Self = TypeVar('Self')


class VideoSource(metaclass=ABCMeta):  # Python 3.6: ContextManager['VideoSource']
    def destroy(self) -> None:
        pass

    @abstractmethod
    def fetch_frame(self) -> Frame:
        pass

    def __enter__(self: Self) -> Self:
        return self

    def __exit__(self, exc_type: Any, exc_value: Any, traceback: Any) -> None:
        self.destroy()


class ThreadedVideoSource(VideoSource):  # pragma: no cover
    def __init__(self, inner: VideoSource) -> None:
        super().__init__()
        self.inner = inner
        self.quit = Queue()  # type: Queue[None]
        self.frames = Queue()  # type: Queue[Frame]
        self.thread = Thread(target=self.target)
        self.thread.start()

    def fetch_frame(self) -> Frame:
        while self.frames.qsize() > 3:
            self.frames.get()
        return self.frames.get()

    def destroy(self) -> None:
        self.quit.put(None)
        self.thread.join()
        self.inner.destroy()

    def target(self) -> None:
        while True:
            try:
                self.quit.get(block=False)
            except Empty:
                pass
            else:
                break
            self.frames.put(self.inner.fetch_frame())


class OpenCVVideoSource(VideoSource):  # pragma: no cover
    def __init__(self, url: str) -> None:
        self.url = url
        self.cap = cv2.VideoCapture(url)

    def destroy(self) -> None:
        self.cap.release()

    def fetch_frame(self) -> Frame:
        ok, image = self.cap.read()
        if not ok:
            logging.warning('Error reading frame')
            while not ok:
                # Re-open the stream to try to get rid of any corrupted state
                self.cap.release()
                self.cap = cv2.VideoCapture(self.url)

                ok, image = self.cap.read()
        now = monotonic()
        return Frame(now, image)
