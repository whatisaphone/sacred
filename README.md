# sacred

Get off the counter, cat.

## Prerequisites

* [Python] 3.6.2
* [Pipenv] 8.3.0
  * This is used for virtualenv management, but unfortunately, 
    it is currently far too buggy (and idiosyncratic) to be realistically used 
    for package management.
* [OpenCV] 3.3.0.10

[Python]: https://www.python.org
[Pipenv]: https://docs.pipenv.org
[OpenCV]: https://opencv.org

## Development

### Initial setup

#### Install packages

    pipenv run pip install -r requirements.base.txt
    pipenv run pip install -r requirements.dev.txt

#### Install OpenCV

OpenCV unfortunately needs to be installed manually because
[there are no prebuilt OpenCV pip packages for ARM][opencv-arm].
Once it's installed, the relevant directory (cv2)
must be manually copied into the relevant path inside `$(pipenv --venv)`.
Sorry.

[opencv-arm]: https://github.com/skvark/opencv-python/issues/13

### Run tests

    pipenv run 'coverage run -m unittest && coverage report'

### Launch the app

    pipenv run python -m sacred <command>

## Deployment

There are two ways to deploy: either using `docker`, or by copying files the 
old fashioned way.

### Using `docker`

Set the `DOCKER_HOST` environment variable to wherever you want to run the app.
Then run this:

    docker-compose -f deploy/env/prod/docker-compose.yaml up --build

### Ye olde manual deploy

Copy the files to your device using the included script:

```sh
deploy/deploy.sh user@host
```

Then, remotely, inside the copied directory, run:

```sh
pipenv run pip install -r requirements.base.txt
```

Then manually start the app from the window manager
(or with the `DISPLAY` env var set appropriately),
using the instructions from [Launch the app](#launch-the-app).
