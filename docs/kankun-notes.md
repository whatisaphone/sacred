ssh to `root@192.168.10.253` with password `p9z34c` (or `password`, or `1234`, or try telnetting in and resetting the password with `passwd`).

In `/etc/config/wireless`:

    config wifi-iface
        option device   radio0
        option network  wwan
        option mode     sta
        option ssid     {SSID}
        option key      {KEY}
        option encryption psk2

In `/etc/config/network`:

    config interface 'wwan'
        option proto 'dhcp'
        option hostname '{HOSTNAME}'

CGI script found [here](https://github.com/homedash/kankun-json).
Place the script into `/www/cgi-bin/json.cgi`, and make sure to `chmod +x`. Then you can control the relay using [http://your-device/cgi-bin/json.cgi]().

Optionally copy `index.html` into `/www/`.

To install opkg, scp the file over,then:

    tar xz -C / -f /root/opkg-rc3.tar.gz
    opkg update