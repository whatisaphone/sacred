$fs = 0.8;

switch_x = 54;
switch_z = 102;
switch_trim = 4;
depth = 10;

// https://www.servocity.com/hs-311-servo
servo_scale = 1.04;
servo_da = servo_scale * 19.82;  // width
servo_db =               13.47;  // height from shaft to mount points
servo_dd =               10.17;  // width between mount points
servo_de = servo_scale * 9.66;   // height above shaft
servo_df = servo_scale * 30.22;  // height below shaft
servo_dg = servo_scale * 11.68;  // depth from back of mount points to where shaft enters body
servo_dh = servo_scale * 26.67;  // depth behind mount points
servo_dm = servo_scale * 39.88;  // height
servo_dx = servo_scale * 3.05;   // depth from front of shaft to where shaft enters body

servo_wires = 4;
thickness = 2;
roundness = 5;
mount_height = (servo_db - servo_de) * 2;
joint_right = servo_de * 2;
jut = 18;
rod_thickness = 4;
rod_hole = 5;
rod_hole_jut = 12;
screw_hole = 2.5;
paper_clip_hole = 1.5;

height = thickness + servo_df + switch_z / 2;

module servo_wrap() {
  rotate([90, 0, 0]) mirror([0, 0, 1]) linear_extrude(thickness)
    difference() {
      square([servo_da + thickness * 2, servo_dm + mount_height * 2]);
      translate([thickness, mount_height])
        square([servo_da, servo_dm]);
    }
}

module servo_mount_holes() {
  translate([-servo_dd / 2, 0, 0]) servo_mount_hole();
  translate([ servo_dd / 2, 0, 0]) servo_mount_hole();
}

module servo_mount_hole() {
  rotate([90, 0, 0]) mirror([0, 0, 1]) linear_extrude(thickness + 0.0001)
    circle(d=screw_hole);
}

module servo_casing() {
  difference() {
    union() {
      translate([0, 0, -mount_height])
        servo_wrap();
    }
    translate([thickness + servo_da / 2, 0, servo_dm + servo_db - servo_de])
      servo_mount_holes();
    translate([thickness + servo_da / 2, 0, -mount_height + servo_db - servo_de])
      servo_mount_holes();
    translate([thickness + (servo_da - servo_wires) / 2, 0, servo_dm - 0.0001])
        cube([servo_wires, depth, mount_height + 0.0002]);
  }
}

module switch_wrap() {
  rotate([90, 0, 0]) mirror([0, 0, 1]) linear_extrude(depth) {
    difference() {
      square([switch_x + thickness * 2, height + thickness]);
      translate([thickness, 0])
        square([switch_x, height]);
    }
    *translate([0, height - switch_z - thickness, 0])
      difference() {
        square([switch_x + thickness * 2, switch_z + thickness * 2]);
        translate([thickness, thickness])
          square([switch_x, switch_z]);
      }
  }
}

module joint(height) {
  rotate([90, 0, 0]) linear_extrude(thickness * 3)
    square([switch_trim, height]);
}

//module centered_around(point) {
//  translate(point) children() translate(-point);
//}

module anchor_left() {
  height = servo_de * 2;
  narrow = height / 3;
  translate([0, -depth, 0]) rotate([0, -90, 0]) mirror([0, 1, 0]) linear_extrude(thickness)
    difference() {
      hull() {
        translate([         narrow + roundness, jut - roundness]) circle(roundness);
        translate([height - narrow - roundness, jut - roundness]) circle(roundness);
        translate([                  roundness, 0])               circle(roundness);
        translate([height          - roundness, 0])               circle(roundness);
      }
      //translate([height / 2, rod_hole_jut]) circle(rod_hole / 2);
      translate([0, rod_hole_jut]) hull() {
        translate([height / 2 - 1, 0]) circle(d=rod_hole);
        translate([height / 2 + 1, 0]) circle(d=rod_hole);
      }
    }
}

module anchor_right() {
  height = servo_de * 2 + mount_height * 2;
  translate([0, -depth, -height / 2]) rotate([0, -90, 0]) mirror([0, 1, 0]) linear_extrude(thickness)
    difference() {
      hull() {
        translate([roundness,          jut - roundness])    circle(roundness);
        translate([height - roundness, jut - roundness])    circle(roundness);
        translate([0,                  -depth + thickness]) square([height, 0.0001]);
      }
      translate([0, rod_hole_jut]) hull() {
        translate([ rod_hole / 2 + thickness * 2,          0]) circle(rod_hole / 2);
        translate([-rod_hole / 2 - thickness * 2 + height, 0]) circle(rod_hole / 2);
      }
    }
}

module rod() {
  rod_gap = 9;
  widen = 7;
  rod_length = 120;
  gap_offset = 5;
  gap_length = 35;
  split1a = rod_length / 2 + gap_offset - gap_length / 2 - widen;
  split1b = rod_length / 2 + gap_offset - gap_length / 2;
  split2a = rod_length / 2 + gap_offset + gap_length / 2;
  split2b = rod_length / 2 + gap_offset + gap_length / 2 + widen;
  mount = 2;
  rod_mid_thickness = rod_thickness + 9;
  difference() {
    union() {
      cylinder(d=rod_thickness, h=rod_length);
      *hull() {
        translate([-widen, rod_thickness / 2, split1a + rod_gap / 2]) rotate([90, 0, 0])
          cylinder(d=rod_gap, h=rod_thickness);
        translate([-widen, rod_thickness / 2, split2b - rod_gap / 2]) rotate([90, 0, 0])
          cylinder(d=rod_gap, h=rod_thickness);
        translate([widen, rod_thickness / 2, split1a + rod_gap / 2]) rotate([90, 0, 0])
          cylinder(d=rod_gap, h=rod_thickness);
        translate([widen, rod_thickness / 2, split2b - rod_gap / 2]) rotate([90, 0, 0])
          cylinder(d=rod_gap, h=rod_thickness);
      }
      hull() {
        translate([0,      0, split1a]) cylinder(d=rod_thickness, h=split2b - split1a);
        translate([-widen, 0, split1b]) cylinder(d=rod_thickness, h=split2a - split1b);
        translate([ widen, 0, split1b]) cylinder(d=rod_thickness, h=split2a - split1b);
      }
      hull() {
        translate([0,  0, mount - 2]) cylinder(d=rod_thickness, h=4);
        translate([-1, 0, mount - 1]) cylinder(d=rod_thickness, h=2);
        translate([1,  0, mount - 1]) cylinder(d=rod_thickness, h=2);
      }
    }
    *translate([-rod_gap / 2, -rod_thickness / 2]) hull() {
      translate([rod_gap / 2, -0.0001, split1b]) rotate([-90, 0, 0]) cylinder(d=rod_gap, h=rod_thickness + 0.0001);
      translate([rod_gap / 2, 0,       split2a]) rotate([-90, 0, 0]) cylinder(d=rod_gap, h=rod_thickness + 0.0001);
    }
    translate([-rod_gap / 2, -rod_thickness / 2, split1b]) hull() {
      cube([rod_gap, rod_thickness, split2a - split1b]);
      dz = rod_gap / 2;
      translate([rod_gap / 2, 0, -dz]) scale([0, 1, 1]) cube([rod_gap, rod_thickness, split2a - split1b + dz * 2]);
    }
    translate([0, rod_thickness / 2 + 0.0001, mount]) rotate([90, 0, 0])
      cylinder(d=paper_clip_hole, h=rod_thickness + 0.0002);
  }
}

module apparatus() {
  translate([switch_trim + thickness, depth - thickness, thickness]) {
    servo_casing();
  }
  translate([thickness, depth, thickness + servo_df - joint_right / 2]) {
    joint(joint_right);
  }
  translate([thickness + switch_trim + thickness, depth, thickness + servo_df]) {
    anchor_right();
  }
  translate([-switch_x - thickness, 0, 0]) {
    switch_wrap();
  }
  translate([-switch_x - switch_trim - thickness, depth, thickness + servo_df - servo_de * 2 / 2]) {
    joint(servo_de * 2);
    rotate([90, 0, 0]) mirror([1, 0, 0]) linear_extrude(depth)
      square([thickness, servo_de * 2]);
    anchor_left();
  }
}

module brace() {
  rotate([90, 0, 0]) mirror([0, 0, 1]) linear_extrude(depth)
    difference() {
      union() {
        square([switch_x + thickness * 2, switch_z - height + thickness]);
        translate([-thickness, switch_z - height + thickness])
          square([switch_x + thickness * 4, switch_z - height + thickness]);
      }
      union() {
        translate([thickness, thickness])
          square([switch_x, switch_z - height + thickness]);
        translate([0, switch_z - height + thickness * 2])
          square([switch_x + thickness * 2, switch_z - height]);
      }
    }
}

color([1.0, 0.5, 0.5])
  apparatus();
color([0.5, 1.0, 0.5]) translate([40, -rod_hole_jut, thickness + servo_df]) rotate([0, -90, 0])
  rod();
color([0.5, 0.5, 1.0]) translate([-switch_x - thickness, 0, height - switch_z - thickness * 2])
  brace();