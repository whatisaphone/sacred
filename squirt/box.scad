width = 85;
length = 57;
height = 33;
thk = 2;
lip = thk * 3;
slot_z = 20;
slot1_y = length - 4;
slot2_y = 25;
slot1_size = 2;
slot2_size = 1.5;

lid_expand = 1.5;
lid_width = width + lid_expand;
lid_length = length + lid_expand;

difference() {
  union() {
    translate([-thk * 2, -thk * 2, -thk])
      cube([width + thk * 4, length + thk * 4, height + thk - lip]);
    translate([-thk, -thk, -thk])
      cube([width + thk * 2, length + thk * 2, height + thk]);
  }
  cube([width, length, height]);
  translate([-thk * 2, -thk * 2 + slot1_y, slot_z])
    cube([thk * 2, slot1_size, height - slot_z]);
  translate([-thk * 2, -thk * 2 + slot2_y, slot_z])
    cube([thk * 2, slot2_size, height - slot_z]);
  translate([width / 2, length / 2, -thk])
    linear_extrude(thk)
      circle(d=20);
}

translate([-thk * 2, -thk * 2, height + thk * 4]) {
  difference() {
    cube([lid_width + thk * 4, lid_length + thk * 4, lip + thk]);
    translate([thk, thk, 0])
      cube([lid_width + thk * 2, lid_length + thk * 2, lip]);
  }
}
