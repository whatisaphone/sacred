#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <Servo.h>

#include "secrets.h"

// const int ledPin = 1;
// const int ledPolarity = HIGH;
// const int servoPin = 0;

WiFiServer server(80);
Servo servo;

void setup() {
  Serial.begin(115200);
  Serial.println("\n\nRotary\n======");

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW ^ ledPolarity);

  connectToWifi();

  server.begin();
}

void connectToWifi() {
  Serial.print("Connecting to SSID ");
  Serial.println(wifiSSID);

  WiFi.hostname(wifiHostname);
  WiFi.begin(wifiSSID, wifiPassword);

  bool lit = LOW ^ ledPolarity;
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    digitalWrite(ledPin, lit = !lit);
    Serial.print(".");
  }
  digitalWrite(ledPin, LOW ^ ledPolarity);

  Serial.println("\nConnected.");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Hostname: ");
  Serial.println(wifiHostname);
}

void loop() {
  WiFiClient client = server.available();
  if (!client)
    return;

  String request = client.readStringUntil('\r');
  readRequestHeaders(client);

  if (request.startsWith("GET / HTTP/"))
    indexView(client);
  else if (request.startsWith("POST /sweep HTTP/"))
    sweepView(client);
  else
    fourOhFourView(client);
}

void readRequestHeaders(WiFiClient client) {
  while (client.available()) {
    String header = client.readStringUntil('\r');
    client.read();  // assume it's '\n'
    if (header == "")
      return;
  }
}

void indexView(WiFiClient client) {
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html;charset=utf-8");
  client.println("Connection: Close");
  client.println("");
  client.println(
    "<!DOCTYPE html>"
    "<meta charset=utf-8>"
    "<title>Rotary</title>"
    "<meta name=viewport content='width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no'>"
    "<h1>Rotary</h1>"
    "<table>"
      "<tr><td><label for=i-angle-a>Angle A</label></td><td><input id=i-angle-a name=angleA value=0 size=2>°</td></tr>"
      "<tr><td><label for=i-angle-b>Angle B</label></td><td><input id=i-angle-b name=angleB value=180 size=2>°</td></tr>"
      "<tr><td><label for=i-time>Time</label></td><td><input id=i-time name=time value=1000 size=2> ms</td></tr>"
      "<tr><td><label for=i-granularity>Granularity</label></td><td><input id=i-granularity name=granularity value=15 size=2> ms</td></tr>"
    "</table>"
    "<div><button id=a-to-b>A &rarr; B</button></div>"
    "<div><button id=b-to-a>B &rarr; A</button></div>"
    "<div id=status></div>"
    "<script>"
      "const inputs = ['i-from', 'i-to', 'i-time', 'i-granularity']"
        ".map(id => document.getElementById(id));"
      "const button = document.querySelector('button');"
      "const status = document.querySelector('#status');"
      "document.getElementById('a-to-b').addEventListener('click', () => {"
        "sweep(true);"
      "});"
      "document.getElementById('b-to-a').addEventListener('click', () => {"
        "sweep(false);"
      "});"
      "function sweep(forwards) {"
        "const data = {"
          "angleFrom: +document.getElementById(forwards ? 'i-angle-a' : 'i-angle-b').value,"
          "angleTo: +document.getElementById(forwards ? 'i-angle-b' : 'i-angle-a').value,"
          "time: +document.getElementById('i-time').value,"
          "granularity: +document.getElementById('i-granularity').value,"
        "};"
        "const xhr = new XMLHttpRequest();"
        "xhr.open('POST', '/sweep');"
        "xhr.send(JSON.stringify(data));"
        "status.textContent = 'Rotating…';"
        "xhr.onreadystatechange = () => {"
          "if (xhr.readyState === 4) {"
            "status.textContent = xhr.status === 200 ? 'Success' : 'Error';"
          "}"
        "};"
      "}"
    "</script>");
}

void sweepView(WiFiClient client) {
  StaticJsonBuffer<200> buffer;
  JsonObject &obj = buffer.parseObject(client.readString());
  int from = obj["angleFrom"];
  int to = obj["angleTo"];
  int time = obj["time"];
  int granularity = obj["granularity"];

  // clamp to sane values
  if (from < 0) from = 0;
  if (from > 180) from = 180;
  if (to < 0) to = 0;
  if (to > 180) to = 180;
  if (time < 100) time = 100;
  if (time > 10000) time = 10000;
  if (granularity < 1) granularity = 1;
  if (granularity > 100) granularity = 100;

  digitalWrite(ledPin, HIGH ^ ledPolarity);
  servo.attach(servoPin);
  sweepServo(&servo, from, to, time, granularity);
  servo.detach();
  digitalWrite(ledPin, LOW ^ ledPolarity);

  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: application/json");
  client.println("Connection: Close");
  client.println("");
  client.println("{\"status\": \"ok\"}");
}

void sweepServo(Servo *servo, double from, double to, double time, double granularity) {
  double t = 0;
  for (;;) {
    servo->write(from + t / time * (to - from));
    if (t >= time)
      break;
    delay(granularity);
    t += granularity;
  }
}

void fourOhFourView(WiFiClient client) {
  client.println("HTTP/1.1 404 Not Found");
  client.println("Content-Type: text/html");
  client.println("Connection: Close");
  client.println("");
  client.println("<!DOCTYPE html>404 Not Found");
}
