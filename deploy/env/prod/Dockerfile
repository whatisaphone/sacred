FROM python:3.5

RUN apt-get -y update && \
    apt-get -y install unzip \
        build-essential cmake pkg-config \
        libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev \
        libavcodec-dev libavformat-dev libswscale-dev libv4l-dev \
        libxvidcore-dev libx264-dev \
        libgtk2.0-dev libgtk-3-dev \
        libcanberra-gtk* \
        libatlas-base-dev gfortran

RUN cd /tmp && \
    curl -L https://github.com/Itseez/opencv/archive/3.3.0.zip > opencv.zip && \
    unzip opencv.zip && \
    curl -L https://github.com/Itseez/opencv_contrib/archive/3.3.0.zip > opencv_contrib.zip && \
    unzip opencv_contrib.zip

RUN pip install pipenv numpy==1.13.3

RUN cd /tmp/opencv-3.3.0/ && \
    mkdir build && \
    cd build && \
    cmake -D CMAKE_BUILD_TYPE=RELEASE \
        -D CMAKE_INSTALL_PREFIX=/usr/local \
        -D OPENCV_EXTRA_MODULES_PATH=/tmp/opencv_contrib-3.3.0/modules \
        -D ENABLE_NEON=ON \
        -D ENABLE_VFPV3=ON \
        -D BUILD_TESTS=OFF \
        -D INSTALL_PYTHON_EXAMPLES=OFF \
        -D BUILD_EXAMPLES=OFF \
        ..
RUN cd /tmp/opencv-3.3.0/build && make
RUN cd /tmp/opencv-3.3.0/build && make install

WORKDIR /opt/sacred

COPY requirements.base.txt /opt/sacred
RUN pip install -r requirements.base.txt

COPY assets /opt/sacred/assets
COPY sacred /opt/sacred/sacred
COPY deploy/env/prod/config.yaml /opt/sacred

CMD ["python", "-m", "sacred", "patrol"]
