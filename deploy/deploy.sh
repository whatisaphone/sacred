#!/usr/bin/env bash

ROOT="$(dirname "$0")/.."

REMOTE="$1"

find "$ROOT" \
        -name 'requirements.base.txt' \
        -o -name '*.py' \
        -o -name 'counter-mask.png' \
    | grep -v /tests/ \
    | xargs tar cz \
    | ssh "$REMOTE" 'mkdir -p sacred && tar xz -C sacred'
